# Flask Back React Front
This demo is to demonstrate how Flask back-end can support React front-end while using the back-end's server.
Here's the step by step [video](https://www.youtube.com/watch?v=YW8VG_U-m48)

## To Run the Demo
1. Clone the repo^^
2. Change directory the to the root folder of the repo.
3. Inside the root folder of OSX terminal type ``` source /env/bin/activate ``` to run the virtual python enviroment.
4. Inside the enviroment make sure to ```pip install flask``` then install reactjs dependencies.
5. Change directory or cd to flask-backend then type ```python3 main.py```
